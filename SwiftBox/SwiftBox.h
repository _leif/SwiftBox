//
//  SwiftBox.h
//  SwiftBox
//
//  Created by Zach Eriksen on 7/2/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for SwiftBox.
FOUNDATION_EXPORT double SwiftBoxVersionNumber;

//! Project version string for SwiftBox.
FOUNDATION_EXPORT const unsigned char SwiftBoxVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SwiftBox/PublicHeader.h>


