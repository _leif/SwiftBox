//
//  SwiftBox.swift
//  SwiftBox
//
//  Created by Zach Eriksen on 7/2/18.
//  Copyright © 2018 oneleif. All rights reserved.
//

import Foundation

struct SwiftBox {
    private var value: Any
    private var kind: Any.Type
    
    init(_ obj: Any) {
        value = obj
        kind = type(of: obj)
    }
    
    func pop<E>() -> E? {
        return open()
    }
    
    mutating func set(_ obj: Any) {
        value = obj
        kind = type(of: obj)
    }
    
    private func open<E>() -> E? {
        return value as? E
    }
}
